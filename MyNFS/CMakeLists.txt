cmake_minimum_required(VERSION 3.12)
project(MyNFS)

set(CMAKE_CXX_STANDARD 14)

add_executable(server Server/Server.cpp Server/Server.h)
add_executable(client Client/Client.cpp Client/Client.h mynfs.h)