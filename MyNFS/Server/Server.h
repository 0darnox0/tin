#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

class Server {
    int opt = true;
    static const int MAX_CLIENTS = 50;
    int sockfd;
    int new_sockfd;
    int max_sd;
    int sd;
    int port;
    socklen_t clielen;
    int addrlen;
    int activity;
    struct sockaddr_in serv_addr;
    struct sockaddr_in clie_addr;
    struct sockaddr_in address;
    fd_set readfds;
    int client_socket[MAX_CLIENTS];


    Server() = delete;

    Server(int port) : port(port) {}

    int start() {
        socket();
        bind();
        listen();
//        accept();
        select();

        close();
    }

    int socket() {
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {  // TODO: check if < or == 0
            perror("Can't open the socket");
            return -1;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
            perror("Can't set opt");
            return -1;
        }

        return 0;
    }

    int bind() {
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(port);

        if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
            perror("Error on binding");
            return -1;
        }

        return 0;
    }

    int listen() {
        if (listen(sockfd, 5) < 0) {
            perror("Can't activate the listening");
            return -1;
        }

        return 0;
    }

    int accept() {
        clielen = sizeof(clie_addr);

        new_sockfd = accept(sockfd, (struct sockaddr *) &clie_addr, &clielen;);
        if (new_sockfd < 0) {
            perror("Error on accept");
            return -1;
        }

        return 0;
    }

    int select() {
        for (int &sd : client_socket) {
            sd = 0;
        }

        addrlen = sizeof(address);

        while (true) {
            //clear the socket set
            FD_ZERO(&readfds);

            //add master socket to set
            FD_SET(sockfd, &readfds);
            max_sd = sockfd;

            //add child sockets to set
            for (int sd : client_socket) {

                //if valid socket descriptor then add to read list
                if (sd > 0) {
                    FD_SET(sd, &readfds);
                }

                //highest file descriptor number, need it for the select function
                if (sd > max_sd) {
                    max_sd = sd;
                }
            }

            //wait for an activity on one of the sockets , timeout is NULL ,
            //so wait indefinitely
            activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);  // TODO: fill with other fd sets

            if ((activity < 0) && (errno != EINTR)) {
                printf("select error");
            }

            //If something happened on the master socket ,
            //then its an incoming connection
            if (FD_ISSET(sockfd, &readfds)) {
                if ((new_sockfd = accept(sockfd, (struct sockaddr *) &address, (socklen_t * ) & addrlen)) < 0) {
                    perror("accept");
                    exit(EXIT_FAILURE);
                }

                //add new socket to array of sockets
                for (int &sd: client_socket) {
                    //if position is empty
                    if (sd == 0) {
                        sd = new_sockfd;
                        printf("Adding to list of sockets as %d\n", sd);

                        break;
                    }
                }
            }

            //else its some IO operation on some other socket
            for (int sd : client_socket) {

                if (FD_ISSET(sd, &readfds)) {
                    // read incoming message
                }
            }
        }
    }

    int close() {
        close(sockfd);
    }
};
