#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

class MyNFS {
    int error = 0;

    int sockfd;
    int port;
    struct socket_addr_in serv_addr;
    struct hostent *server;

    int connect(char *host, int port) {
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {
            perror("Can't open the socket");
            // TODO: set error var appropriately
            return -1;
        }

        server = gethostbyname(host);
        if (server == nullptr) {
            perror("No such host");
            // TODO: set error var appropriately
            return -1;
        }

        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
        serv_addr.sin_port = htons(port);
        if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
            perror("Can't connect to the server");
            // TODO: set error var appropriately
            return -1;
        }

        return 0;
    }

    int disconnect();

    /* Return a file descriptor on success. On error, -1 is returned, and errno is set appropriately. */
    int open(char *path, int oflag, int mode);


    /* On success, the number of bytes read is returned (zero indicates end of file), and the file position is
     * advanced by this number. It is not an error if this number is smaller than the number of bytes requested;
     * this may happen for example because fewer bytes are actually available right now (maybe because we were
     * close to end-of-file, or because we are reading from a pipe, or from a terminal), or because read() was
     * interrupted by a signal. On error, -1 is returned, and errno is set appropriately. In this case it is left
     * unspecified whether the file position (if any) changes.*/
    int read(int fd, void *buf, int count);


    /* On success, the number of bytes written is returned (zero indicates nothing was written). On error, -1
     * is returned, and errno is set appropriately.
     * If count is zero and fd refers to a regular file, then write() may return a failure status if one of the errors
     * below is detected. If no errors are detected, 0 will be returned without causing any other effect. If count is
     * zero and fd refers to a file other than a regular file, the results are not specified. */
    int write(int fd, const void *buf, int count);


    /*  Upon successful completion, lseek() returns the resulting offset
    location as measured in bytes from the beginning of the file.  On
    error, the value -1 is returned and errno is set to indicate
    the error. */
    int lseek(int fd, int offset, int whence);


    /* Returns zero on success. On error, -1 is returned, and errno is set appropriately. */
    int close(int fd);


    /* On success, zero is returned. On error, -1 is returned, and errno is set appropriately. */
    int unlink(char *path);


    /* Return a directory descriptor on success. On error, -1 is returned, and errno is set appropriately. */
    int opendir(char *path);


    /* If the end of the directory stream is reached, NULL is returned and errno is not changed. If an error occurs, NULL is returned and errno is set appropriately. */
    char *readdir(int dirfd);


    /* Returns 0 on success. On error, -1 is returned, and errno is set appropriately. */
    int closedir(int dirfd);


    /* On success, zero is returned. On error, -1 is returned, and errno is set appropriately. */
    int fstst(int fd, struct stat *buf);
};



/* Zamiast dirent w readsir możemy zwracać same nazwy plików
 * struct dirent {
    ino_t          d_ino;       // Inode number
    off_t          d_off;       // Not an offset; see below
    unsigned short d_reclen;    // Length of this record
    unsigned char  d_type;      // Type of file; not supported by all filesystem types
    char           d_name[256]; // Null-terminated filename
};
 */

/* struct stat {
        dev_t     st_dev;     // ID of device containing file
        ino_t     st_ino;     // inode number
        mode_t    st_mode;    // protection
        nlink_t   st_nlink;   // number of hard links
        uid_t     st_uid;     // user ID of owner
        gid_t     st_gid;     // group ID of owner
        dev_t     st_rdev;    // device ID (if special file)
        off_t     st_size;    // total size, in bytes
        blksize_t st_blksize; // blocksize for file system I/O
        blkcnt_t  st_blocks;  // number of 512B blocks allocated
        time_t    st_atime;   // time of last access
        time_t    st_mtime;   // time of last modification
        time_t    st_ctime;   // time of last status change
    };
*/